package com.exmp.basicnavigationpattern

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createSubFragment1()
    }

//    В этом методе происходит прекрипление SubFragment1 к контейнеру в активность.
//    Обрати внимание, что контейнер в верстке растянут на весь экран.

    private fun createSubFragment1() {
        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

//        Тут происходит вызов метода newInstance, который определен в классе SubFragment1 !!!
        val subFragment1 = SubFragment1.newInstance() // TODO: Внимание на эту строку!
        fragmentTransaction.replace(R.id.sub_fragment_container, subFragment1)
        fragmentTransaction.commit()
    }
}
