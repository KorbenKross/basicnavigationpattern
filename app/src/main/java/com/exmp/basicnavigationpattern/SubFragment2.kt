package com.exmp.basicnavigationpattern

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.sub_fragment_2.*

class SubFragment2 : Fragment() {

    companion object {
        fun newInstance(): SubFragment2 {
            return SubFragment2()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sub_fragment_2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button2.setOnClickListener {
            val fragmentManager = this.fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            val subFragment3 = SubFragment3.newInstance()
            fragmentTransaction.replace(R.id.sub_fragment_container, subFragment3)
            fragmentTransaction.commit()
        }
    }
}