package com.exmp.basicnavigationpattern

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.sub_fragment_1.*

class SubFragment1 : Fragment() {

//    Тут происходит переопределение. Это нужно для того, чтобы вызвать аргументы
//    , либо выполнить какие либо действия.
//    Данный код отрабатывает ДО onCreate() !!!
    companion object {
        fun newInstance(): SubFragment1 {
            return SubFragment1()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sub_fragment_1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button1.setOnClickListener {
            val fragmentManager = this.fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            val subFragment2 = SubFragment2.newInstance()
            fragmentTransaction.replace(R.id.sub_fragment_container, subFragment2)
            fragmentTransaction.commit()
        }
    }
}