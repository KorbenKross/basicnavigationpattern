package com.exmp.basicnavigationpattern

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.sub_fragment_3.*

class SubFragment3 : Fragment() {

    companion object {
        fun newInstance(): SubFragment3 {
            return SubFragment3()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sub_fragment_3, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button3.setOnClickListener {
            val fragmentManager = this.fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            val subFragment1 = SubFragment1.newInstance()
            fragmentTransaction.replace(R.id.sub_fragment_container, subFragment1)
            fragmentTransaction.commit()
        }
    }
}